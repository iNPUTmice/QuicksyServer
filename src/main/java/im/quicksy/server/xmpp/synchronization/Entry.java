/*
 * Copyright 2018 Daniel Gultsch
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package im.quicksy.server.xmpp.synchronization;

import com.google.common.base.MoreObjects;
import java.util.Collection;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import rocks.xmpp.addr.Jid;

@XmlRootElement
public class Entry implements Comparable<Entry> {

    @XmlAttribute private String number;

    @XmlElement(name = "jid", namespace = PhoneBook.NAMESPACE)
    private Collection<Jid> jids;

    private Entry() {}

    public Entry(String number, final Collection<Jid> jids) {
        this.number = number;
        this.jids = jids;
    }

    public String getNumber() {
        return number;
    }

    public Collection<Jid> getJids() {
        return jids;
    }

    @Override
    public int compareTo(Entry entry) {
        return number.compareTo(entry.number);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("number", number).add("jids", jids).toString();
    }
}
